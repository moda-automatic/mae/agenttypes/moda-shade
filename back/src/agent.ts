import {
	Agent,
	apiAddress,
} from '../../../utilities/agent/output';
import { Transaction } from '../../../utilities/transaction/output';
import axios from 'axios';

let agent: Agent = new Agent();
let controlPosition: number;
console.log('NEW AGENT CREATEDEDEDEDEDEDED');

agent.onCommand = (commandData) => {
	switch (commandData.name) {
		case 'id':
			onId(commandData);
			break;
		case 'pair':
			onPair(commandData);
			break;
		case 'ping':
			onPing(commandData);
			break;
		case 'position':
			onPosition(commandData);
			break;
		// TODO: Implement calibration commands.
		case 'calibrate':
			console.log(
				'CALIBRATE COMMAND. NOT YET IMPLEMENTED.'
			);
			break;
	}
};

async function onId(commandData) {
	let commandId: number = commandData.protocol.props.id;
	let commandResponse = await axios.post(
		`${apiAddress}/transaction`,
		{
			bytes: [
				commandId,
				agent.initialState.maeProps.addressPart1,
				agent.initialState.maeProps.addressPart2,
			],
		}
	);

	let responseId: number = parseInt(
		commandResponse.data.split(',')[0]
	);
	if (responseId - Transaction.errorOffset == commandId) {
		agent.sendError("Error retrieving the agent's id.");
	} else if (
		responseId - Transaction.ackOffset ==
		commandId
	) {
		agent.sendAck(
			"The agent's id was successfully retrieved."
		);
	}
}

async function onPair(commandData) {
	let commandId: number = commandData.protocol.props.id;
	let commandResponse = await axios.post(
		`${apiAddress}/transaction`,
		{
			bytes: [
				commandId,
				agent.initialState.maeProps.addressPart1,
				agent.initialState.maeProps.addressPart2,
				0,
				0,
				0,
				0,
			],
		}
	);

	let responseId: number = parseInt(
		commandResponse.data.split(',')[0]
	);
	if (responseId - Transaction.errorOffset == commandId) {
		agent.sendError('Error pairing with the agent.');
	} else if (
		responseId - Transaction.ackOffset ==
		commandId
	) {
		agent.sendAck('The agent was paired.');
	}
}

async function onPing(commandData) {
	let commandId: number = commandData.protocol.props.id;
	let commandResponse = await axios.post(
		`${apiAddress}/transaction`,
		{
			bytes: [
				commandId,
				agent.initialState.maeProps.addressPart1,
				agent.initialState.maeProps.addressPart2,
			],
		}
	);

	let responseId: number = parseInt(
		commandResponse.data.split(',')[0]
	);
	if (responseId - Transaction.errorOffset == commandId) {
		agent.sendError('Error pinging the agent.');
	} else if (
		responseId - Transaction.ackOffset ==
		commandId
	) {
		agent.sendAck('pong');
	}
}

/**
 *
 */
async function onPosition(commandData) {
	let commandId: number = commandData.protocol.props.id;

	/**
	 * Build up a setPosition command
	 */
	let commandBytes: number[] = [
		commandId,
		agent.initialState.maeProps.addressPart1,
		agent.initialState.maeProps.addressPart2,
		/**
		 * Priming the payload with a 0 to select the zeroeth motor,
		 * as is required by the current firmware.
		 *
		 * TODO: Discuss this with Max about whether or not this
		 * should make it's way to production.
		 */
		0,
	];

	/** Save the specified position. */
	controlPosition = commandData.args[0];

	/**
	 * Add a byte for each argument received with
	 * the command into the transaction.
	 */
	commandData.args.forEach((arg) => {
		commandBytes.push(parseInt(arg));
	});

	/** Send the command. */
	let commandResponse = await axios.post(
		`${apiAddress}/transaction`,
		{
			bytes: commandBytes,
		}
	);

	/** Store the response's command id. */
	let responseId: number = parseInt(
		commandResponse.data.split(',')[0]
	);

	if (responseId - Transaction.errorOffset == commandId) {
		/** Error */
		agent.sendError(commandResponse.data);
	} else if (
		responseId - Transaction.ackOffset ==
		commandId
	) {
		/** Ack */
		agent.sendAck(commandResponse.data);
		/**
		 * TODO: add logic to monitor the position change and send out state updates
		 * until the shade has reached the "controlPosition".
		 *
		 * There's a chance that this exact plan has some issues and the logic
		 * may need to be a bit more robust than described here.
		 */
		trackPositionChange();
	}
}

/**
 * Sen the getPosition command on an interval until the shade has
 * reached the desired position.
 *
 * TODO: we'll want to do some error checking here, and probably
 * safeguard against this running forever.
 */
async function trackPositionChange() {
	let getPositionInterval = setInterval(async () => {
		let position: number = await getPosition();
		if (position == controlPosition) {
			/** TODO: clean up and exit this process */
			console.log('control position reached');
			clearInterval(getPositionInterval);
		}
	}, 250);
}

async function getPosition() {
	/**
	 * Build up a getPosition command
	 */
	let commandBytes: number[] = [
		3,
		agent.initialState.maeProps.addressPart1,
		agent.initialState.maeProps.addressPart2,
		0,
	];

	/** Send it. */
	let commandResponse = await axios.post(
		`${apiAddress}/transaction`,
		{
			bytes: commandBytes,
		}
	);

	/** Store the byte representing the position. */
	let newPosition: number = commandResponse.data.split(
		','
	)[7];

	/** Make a copy of the Agent's initial state. */
	let newState = { ...agent.initialState };

	/** Create props object if it doesn't exist. */
	/** TODO: this isn't great to have here. Not sure how to solve now though. */
	if (newState.props == null) {
		newState.props = {};
	}

	/** Save the new position. */
	newState.props.position = newPosition;

	/** TODO: send a state update with the new position */
	agent.sendState(newState);

	return newPosition;
}
