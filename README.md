# Moda Shade Source Code

This repo is split into `front` and `back` directories, which is hopefully self explanatory. 

- Front projects are [Svelte](https://svelte.dev/) projects with [Storybook](https://storybook.js.org/) for inspecting UI.
- Back end projects are Node.js projects within which Moda provided base classes are extended to abstract integration of unique functionality into Moda Boxes.