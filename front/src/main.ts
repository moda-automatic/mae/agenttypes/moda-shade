import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		agentState: {
			name: 'unnamed-agent-1'
		},
		screen: 'controls'
	}
});

export default app;