import { writable } from 'svelte/store';

export const controlPositionStore = writable(0);
export const touchpadHorizPositionStore = writable(0);
export const isBeingAdjustedStore = writable(false);
